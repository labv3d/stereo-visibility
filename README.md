# Occlusion

## Occlusion masking for stereo disparity maps

Compute an accurate occlusion map from a disparity map.

The map will  contain the  percentage of visibility of  a pixel, from 0 to 100% (intensity 255),  encoded as a png image.

## Requirements 

This program is in  C++,  requires *cmake*, and  OpenCV.
Debian packages:

* *cmake*
* *opencv-devel*


## Installing

```
mkdir build
cd build
cmake  ..
make
sudo make install
```

## Usage

Use -h for help.
```
stereovis [-h] [-pfm disp.pfm] [-o outname.png] [-ob outbinary] [-od outdisp]
```

## Examples

Read a pfm disparity, save the occlusion mask (-o), the disparity as png (-od), and the occlusion mask (-ob)
```
stereovis -pfm 0006.pfm 0 1 -o occ.png -od disp.png 64 0 -ob mask.png
```
Do it  a second time,  but loading the  previously saved disparity
```
stereovis -png disp.png 0 64 -o occ2.png -od d2.png 500 0 -ob mask2.png
```
results should be the same, except for the disparity  d2.png which  is scaled  more for  viewing.


## License
GPL V3.

