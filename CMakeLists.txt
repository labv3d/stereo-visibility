
cmake_minimum_required(VERSION 2.8)


project( stereovis )

find_package( OpenCV REQUIRED )

include_directories( ${OpenCV_INCLUDE_DIRS} )

add_executable( stereovis stereovis.cpp )
target_link_libraries( stereovis ${OpenCV_LIBS} )





