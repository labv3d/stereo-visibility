//#include "opencv2/aruco.hpp"
//#include "opencv2/objdetect.hpp"
//#include "opencv2/imgproc.hpp"
//#include "opencv2/highgui.hpp"
//#include "opencv2/videoio.hpp"
//#include "opencv2/calib3d.hpp"
#include  <opencv2/opencv.hpp>
#include <string>
#include <iostream>
#include <sys/time.h>

using namespace std;
using namespace cv;

double horloge() {
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return((double)tv.tv_sec + tv.tv_usec/1000000.0);
}

Mat readPFM(const char *name,float mul,float add) {
    printf("read  ( %s - %f ) / %f\n",name,add,mul);
    FILE *F=fopen(name,"r");
    if( F==NULL )  { printf("unable to open %s\n",name);exit(0); }

    int rgb=-1;
    int w,h;
    float scale;
    int little;

    char s[200];
    s[0]=0;
    fgets(s, 200 , F);
    if( strncmp(s,"Pf",2)==0  ) rgb=0;
    if( strncmp(s,"PF",2)==0  ) rgb=1;
    fgets(s, 200 , F);
    sscanf(s," %d %d",&w,&h);
    fgets(s, 200 , F);
    sscanf(s," %f",&scale);

    if( scale<0  ) little=1;  else  little=0;
    scale=fabs(scale);

    printf("RGB=%d w=%d  h=%d  little=%d scale=%f\n",rgb,w,h,little,scale);


    Mat  frame(h,w,CV_32FC1);

    float *p=(float *)frame.data;
    int  k=fread(p,sizeof(float),w*h,F);
    printf("got  %d\n",k);
    fclose(F);

    float  m=0.0;
    for(int i=0;i<w*h;i++) {
        float  v=(p[i]-add)/mul;
        if(  p[i]>m ) m=v;
    }
    printf("max %f\n",m);

    return frame;
}

Mat read64(const char *name,float mul,float add) {
    Mat img=imread(name,-1);
    //printf("read chan=%d type is %d, %d \n",img.channels(),img.type(),CV_16U);
    printf("read ( %s - %f) / %f\n",name,add,mul);

    Mat frame(img.rows,img.cols,CV_32FC1);

    if( img.type()==CV_16U ) {
        float *p=(float *)frame.data;
        unsigned short *q=(unsigned short *)img.data;
        for(int i=0;i<img.rows*img.cols;i++,p++,q++) {
            *p=((float)(*q)-add)/mul;
            //printf("%5d -> %12.6f\n",*q,*p);
        }
    }else{
        printf("Disparity must be 16 bit grayscale png\n");
        exit(0);
    }
    return frame;
}

// save une  float gray en  png *mult+add
void save64(const char *name,Mat m,float mul,float add) {
    printf("save ( %s * %f ) + %f\n",name,mul,add);
    Mat r(m.rows,m.cols,CV_16UC1);
    float *p=(float *)m.data;
    unsigned  short *q=(unsigned  short *)r.data;
    for(int i=0;i<m.rows*m.cols;i++) {
         int v=round(p[i]*mul+add);
         if( v<0 ) v=0;
         if( v>65535  )  v=65535;
         q[i]=v;
    }
    imwrite(name,r);
}

typedef struct {
    float xR1;
    float xR2;
    float disp;
    int xL;
} face;

int nbF,maxF;
face *F;

void dump(face *f)  {
    printf("xL=%4d d=%12.6f  xR=%12.6f : %12.6f\n",f->xL,f->disp,f->xR1,f->xR2);
}

//  tri par position du debut d'intervalle (xR1)
int cmpF(const void *a,const void *b) {
    face *fa=(face *)a;
    face *fb=(face *)b;

    if( fa->xR1 < fb->xR1  ) return(-1);
    if( fa->xR1 > fb->xR1  ) return(1);
    return(0);
}

// ajuste a  en fonction  de b (qui a une disparite>a,  en avant )
// retourne 1 si changement,  0 sinon
int reduce(face *a,face *b) {
    if( a->disp ==  b->disp  ) return 0;
    if( a->disp > b->disp ) { return reduce(b,a); } 

    float a1=a->xR1;
    float a2=a->xR2;
    float b1=b->xR1;
    float b2=b->xR2;
    // b est en avant  de a
    // a1.....a2  b1....b2
    // b1.....b2  a1....a2
    if( a2<b1 || b2<a1 ) return 0;
    // a1...b1...a2...b2 ->  a1..b1  b1..b2
    if( a1<b1 && b1<a2 && a2<=b2 ) {
        //printf("from (%f,%f)(%f,%f)",a->xR1,a->xR2,b->xR1,b->xR2);
        a->xR2=b1;
        //printf("to (%f,%f)(%f,%f)\n",a->xR1,a->xR2,b->xR1,b->xR2);
        return 1;
    }
    // b1...a1...b2...a2 ->  b2..a2  b1..b2
    if( b1<a1 && a1<b2 && b2<=a2 ) {
        //printf("from (%f,%f)(%f,%f)",a->xR1,a->xR2,b->xR1,b->xR2);
        a->xR1=b2;
        //printf("to (%f,%f)(%f,%f)\n",a->xR1,a->xR2,b->xR1,b->xR2);
        return 1;
    }
    // b1...a1...a2...b2 ->  chop a
    if( b1<=a1 && a2<=b2 ) {
        //printf("from (%f,%f)(%f,%f)",a->xR1,a->xR2,b->xR1,b->xR2);
        a->xR1=100000.0;a->xR2=100000.0;
        a->disp=-1.0;
        //printf("to delete\n");
        return 1;
    }
    // a1...b1...b2...a2 ->  a1..b1  et  b2..a2,   b1..b2
    if( a1<b1 && b2<a2 ) {
        //printf("cut!!  (%f,%f)(%f,%f)\n",a->xR1,a->xR2,b->xR1,b->xR2);
        //printf("from (%f,%f)(%f,%f)\n",a->xR1,a->xR2,b->xR1,b->xR2);
        if( nbF==maxF ) { printf("OOM!");exit(0); }
        F[nbF].xL=a->xL;
        F[nbF].disp=a->disp;
        F[nbF].xR1=b->xR2;
        F[nbF].xR2=a->xR2;
        a->xR2=b->xR1;
        face *c=F+nbF;
        //printf("  to (%f,%f)(%f,%f)\n",a->xR1,a->xR2,b->xR1,b->xR2);
        //printf("  to (%f,%f)(%f,%f)\n",c->xR1,c->xR2,c->xR1,c->xR2);
        nbF++;
        return 1;

    }
    return 0;
}

void clean() {
    //  remove deleted (disp<0)
    for(int i=0;i<nbF;i++) {
        if( F[i].disp<0 ) {
            printf("[%d] deleted\n",i);
            F[i]=F[nbF-1];
            nbF--;
        }
    }
}

Mat occ(Mat imd) {
    int w=imd.cols;
    int h=imd.rows;
    Mat imo(h,w,CV_8UC1,Scalar(0));

    maxF=w*3; // pour les "cut"... 
    F=(face *)malloc(sizeof(face)*maxF);
    float *p=(float *)imd.data;
    unsigned char *q=(unsigned char *)imo.data;

    for(int y=0;y<h;y++) {

        // extract  from current  line
        for(int i=0;i<w;i++) {
            float d=p[y*w+i];
            //printf("%3d %3d : d=%12.6f\n",y,i,d);
            F[i].xL=i;
            F[i].disp=d;
            F[i].xR1=F[i].xL-d-0.5;
            F[i].xR2=F[i].xL-d+0.5;
        }
        nbF=w;

        int c;
        do {
            // sort par xR1
            qsort(F,nbF,sizeof(face),cmpF);

            //if( y==0  ) {for(int i=0;i<w;i++) { dump(F+i); }}

            c=0;
            for(int i=1;i<w-1;i++) {
                c+=reduce(F+i,F+(i+1));
                c+=reduce(F+i,F+(i-1));
            }
            //printf("c=%d nbF=%d\n",c,nbF);

            for(int i=0;i<w;i++) {
                // cas special.. bord de l'image
                // a1..a2..0 -> delete
                if( F[i].xR2<0. ) {
                    F[i].xR1=100000.0;F[i].xR2=100000.0;
                    F[i].disp=-2.;c+=1; }
                // a1..0..a2 ->  0..a2
                if( F[i].xR1<0. ) { F[i].xR1=0.;c+=1; }
            }

            //clean();
        } while( c>0 );

        // extraction des informations...
        for(int i=0;i<nbF;i++) {
            //if( F[i].xL<0 || F[i].xL>=w ) continue;
            int v=(int)((F[i].xR2-F[i].xR1)*255+0.5);
            q[y*w+F[i].xL]+=v;
        }
    }

    return imo;
}

void binary(Mat m) {
    unsigned char *p=m.data;
    for(int i=0;i<m.rows*m.cols;i++,p++) {
	    if(  *p>=128 )  *p=255;  else *p=0;
    }

}


int main(int argc, char *argv[]) {
double start=horloge();

char namepng[500];
char name[500];
char nameOut[500];
char nameOutBin[500];
char nameOutDisp[500];
float omul,oadd; // for disparity png  output
float mul,add; //  for png input only

    namepng[0]=0;
	name[0]=0;
	nameOut[0]=0;
	nameOutBin[0]=0;
	nameOutDisp[0]=0;
    omul=64.0;
    oadd=0.0;
    mul=64.0;
    add=0.0;
	//strcpy(name,"/mnt/efs/datasets/FlyingThings3D/disparity/TRAIN/A/0047/left/0006.pfm");

	for(int i=1;i<argc;i++) {
		if( strcmp("-h",argv[i])==0 ) {
            printf("Usage:  %s [-h] [-pfm disp.pfm 1 0]  [-png disp.png 64 0] [-o occ.png] [-ob mask.png] [-od outdisp.png 64 0]\n",argv[0]);
            printf("-pfm disp.pfm a b -> read a pfm disparity file,  (d-b)/a\n");
            printf("-png disp.png a b -> read a png disparity file,  (d-b)/a\n");
            printf("-od disp.png a b -> save a png disparity file,  d*a+b\n");
            printf("-o occ.png -> occlusion  mask, 0..255 =0%%..100%% visible\n");
            printf("-ob mask.png -> occlusion mask, 0/255 =<50%%/>=50%% visible\n");
        }
		if( strcmp("-pfm",argv[i])==0 && i+3<argc) {
			strcpy(name,argv[i+1]);
            mul=atof(argv[i+2]);
            add=atof(argv[i+3]);
			i+=3;continue;
		}
		if( strcmp("-png",argv[i])==0 && i+3<argc) {
			strcpy(namepng,argv[i+1]);
            mul=atof(argv[i+2]);
            add=atof(argv[i+3]);
			i+=3;continue;
        }
		if( strcmp("-o",argv[i])==0 && i+1<argc) {
			strcpy(nameOut,argv[i+1]);
			i+=1;continue;
		}
		if( strcmp("-ob",argv[i])==0 && i+1<argc) {
			strcpy(nameOutBin,argv[i+1]);
			i+=1;continue;
		}
		if( strcmp("-od",argv[i])==0 && i+3<argc) {
			strcpy(nameOutDisp,argv[i+1]);
            omul=atof(argv[i+2]);
            oadd=atof(argv[i+3]);
			i+=3;continue;
		}
	}

	if(  name[0]==0 && namepng[0]==0  )  { printf("need input (-pmf  or-png)\n");exit(0);}
	if(  name[0]!=0 && namepng[0]!=0  )  { printf("-pmf  and -png  are exclusive\n");exit(0);}

	Mat frame;
    if( name[0] ) {
        frame=readPFM(name,mul,add);
    }else{
        frame=read64(namepng,mul,add);
    }
    //frame=imread("/data/datasets/FlyingThings3D/frames_finalpass/TRAIN/A/0000/left/0006.png", IMREAD_COLOR);
    //frame=imread("/data/datasets/sceneflow/Sampler/FlyingThings3D/disparity/0006.pfm", IMREAD_COLOR);

    //frame=readPFM("/data/datasets/sceneflow/Sampler/FlyingThings3D/disparity/0006.pfm");

    if(  nameOutDisp[0] ) {
	    save64(nameOutDisp,frame,omul,oadd);
    }

    Mat imo=occ(frame);

    if( nameOut[0]  ) {
	imwrite(nameOut,imo);
    }
    if( nameOutBin[0] ) {
	binary(imo);
	imwrite(nameOutBin,imo);
    }

    //imshow("Aruco", frame);
    //waitKey(0);

}

